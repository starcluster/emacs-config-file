;; Elpa and Melpa needed------------------------------------------------------------------
(require 'package)
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

;; Loading theme--------------------------------------------------------------------------
(load-theme 'tango-dark t)
;; (load-theme 'whiteboard t)

;; OCaml support--------------------------------------------------------------------------
;; (load "~/.opam/default/share/emacs/site-lisp/tuareg-site-file")

;; Org config-----------------------------------------------------------------------------
(setq org-log-done 'time)

(setq org-latex-pdf-process (list
   "latexmk -pdflatex='lualatex -shell-escape -interaction nonstopmode' -pdf -f  %f"))

;; (with-eval-after-load 'ox
;;   (require 'ox-hugo))

(defun my/insert-math ()
    (interactive)
  (insert "\\( \\)")
  (backward-char)
  (backward-char)
  (backward-char))

(defun my/insert-math-binding ()
  (local-set-key (kbd "C-x /") #'my/insert-math))

;; Autocompletion-------------------------------------------------------------------------

;; /!\ It is necessary to install the package auto-complete:
;; M-x package-refresh-contents RET
;; M-x package-install RET autocomplete RET


(defvar myPackages
  '(auto-complete))

;; install all packages in list
(mapc #'(lambda (package)
    (unless (package-installed-p package)
      (package-install package)))
      myPackages)


(require 'auto-complete-config)
(ac-config-default)
(add-to-list 'ac-modes 'python-mode)

;; Shortcut Latex-------------------------------------------------------------------------

(defun insert-math ()
  (interactive)
  (insert "\\( \\)"))

(global-set-key (kbd "C-c /") 'insert-math)

;; Abbreviation python--------------------------------------------------------------------

(define-abbrev-table 'python-mode-abbrev-table
  '(
    ("npy" "import numpy as np")
    ("mplt" "import matplotlib.pyplot as plt")
    ("printf" "print(f\"{}\")")
    ("pltdecorate" "plt.xlabel(\"\",fontsize=20)
plt.ylabel(\"\",fontsize=20)
plt.title(\"\",fontsize=20)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.savefig(\".pdf\",format=\"pdf\",bbox_inches='tight')"
     )
    ("obj" "class Object():
    def __init__(self):")
    ("ifmain" "if __name__ == \"__main__\":")
    ("pltsub" "fig, axs = plt.subplots(2, 2)
axs[0, 0].plot(x, y1) # TL
axs[0, 0].set_title(\"Figure 1\")
axs[0, 1].plot(x, y2) # TR
axs[0, 1].set_title(\"Figure 2\")
axs[1, 0].plot(x, y3) # BL
axs[1, 0].set_title(\"Figure 3\")
axs[1, 1].plot(x, y4) # BR
axs[1, 1].set_title(\"Figure 4\")
plt.tight_layout()
plt.show()")
      ("plttex" "plt.rc(\"text\", usetex=True)
plt.rc(\"font\", family=\"serif\", serif=\"Computer Modern\")"
       )
            ("rich" "from rich import traceback
traceback.install()"
     )))

;; Greek letter for physics programming---------------------------------------------------


(define-abbrev global-abbrev-table "lalpha" "ɑ")
(define-abbrev global-abbrev-table "lbeta" "β")
(define-abbrev global-abbrev-table "lgamma" "γ")
(define-abbrev global-abbrev-table "ldelta" "δ")
(define-abbrev global-abbrev-table "lepsilon" "ε")
(define-abbrev global-abbrev-table "lzeta" "ζ")
(define-abbrev global-abbrev-table "leta" "η")
(define-abbrev global-abbrev-table "ltheta" "θ")
(define-abbrev global-abbrev-table "llambda" "λ")
(define-abbrev global-abbrev-table "lmu" "μ")
(define-abbrev global-abbrev-table "lnu" "ν")
(define-abbrev global-abbrev-table "lxi" "ξ")
(define-abbrev global-abbrev-table "lpi" "π")
(define-abbrev global-abbrev-table "lrho" "ρ")
(define-abbrev global-abbrev-table "lsigma" "σ")
(define-abbrev global-abbrev-table "ltau" "τ")
(define-abbrev global-abbrev-table "lupsilon" "υ")
(define-abbrev global-abbrev-table "lphi" "φ")
(define-abbrev global-abbrev-table "lchi" "χ")
(define-abbrev global-abbrev-table "lpsi" "ψ")
(define-abbrev global-abbrev-table "lomega" "ω")

(define-abbrev global-abbrev-table "lGamma" "Γ")
(define-abbrev global-abbrev-table "lDelta" "Δ")
(define-abbrev global-abbrev-table "lTheta" "Θ")
(define-abbrev global-abbrev-table "lLambda" "Λ")
(define-abbrev global-abbrev-table "lXi" "Ξ")
(define-abbrev global-abbrev-table "lPi" "Π")
(define-abbrev global-abbrev-table "lSigma" "Σ")
(define-abbrev global-abbrev-table "lPhi" "Φ")
(define-abbrev global-abbrev-table "lPsi" "Ψ")
(define-abbrev global-abbrev-table "lOmega" "Ω")

(setq-default abbrev-mode t)


;; Folding code---------------------------------------------------------------------------

(require 'hideshow)
(add-hook 'prog-mode-hook #'hs-minor-mode)

(add-hook 'python-mode-hook #'hs-minor-mode)
(add-hook 'python-mode-hook #'hs-hide-all)

(add-hook 'c-mode-common-hook #'hs-minor-mode)
(add-hook 'c-mode-common-hook #'hs-hide-all)

(add-hook 'tuareg-mode-hook #'hs-minor-mode)
(add-hook 'tuareg-mode-hook #'hs-hide-all)

(eval-after-load "hideshow"
  '(progn
     (define-key hs-minor-mode-map (kbd "C-f") 'hs-toggle-hiding)))

;; Formatting section of codes

(defun insert-dashes ()
  ;; Insert dash when programming with lisp to create sections.
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (not (eobp))
      (when (looking-at "^;;;")
      (end-of-line)
      (let ((len (- 90 (current-column))))
        (when (> len 0)
          (insert (make-string len ?-)))))
      (forward-line 1))))

(defun insert-sharps ()
  ;; Insert # when programming with python to create sections.
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (not (eobp))
      (when (looking-at "^###")
      (end-of-line)
      (let ((len (- 90 (current-column))))
        (when (> len 0)
          (insert (make-string len ?#)))))
      (forward-line 1))))

;; random code syntaxcally correct
;; mini tree of the project
;; header of a function when hovering + C-h
;; ya snippet

(defun run-black-on-buffer ()
  "Format the current buffer with Black."
  (interactive)
  (shell-command-on-region (point-min) (point-max) "python -m black -q -" (current-buffer) t "*black-errors*" t)
  (toggle-frame-fullscreen))

(defun my-python-save-and-execute ()
  (interactive)
  (let ((current-pos (point)))
    (run-black-on-buffer)
    (hs-hide-all)
    (goto-char current-pos))
  (save-buffer))

(defun my-python-mode-hook ()
  (when (string-equal major-mode "python-mode")
    (local-set-key (kbd "C-x C-b") 'my-python-save-and-execute)))

(add-hook 'python-mode-hook 'my-python-mode-hook)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(quickrun rust-mode ox-hugo tuareg auto-complete)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'quickrun)
(global-set-key (kbd "C-c C-e") 'quickrun)
